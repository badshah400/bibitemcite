This mediawiki extension converts LaTeX bibitem entries placed inside the `<bibitem>` tag into `<ref>` items for further processing by the [Cite extension](https://www.mediawiki.org/wiki/Extension:Cite).

## Installation ##

* Ensure that the [Cite extension](https://www.mediawiki.org/wiki/Extension:Cite) is installed and working first.
* Clone the git repository into a directory called `BibitemCite` inside your `extensions/` directory:

```
git clone https://badshah400@bitbucket.org/badshah400/bibitemcite.git BibitemCite
```

* Add the following at the bottom of your [LocalSettings.php](https://www.mediawiki.org/wiki/Special:MyLanguage/Manual:LocalSettings.php)

```
wfLoadExtension( 'BibitemCite' );
```
## Usage ##

Place a valid LaTeX bibitem entry inside a `<bibitem>` anchor and this extension will automatically convert it to a `<ref>` with its `name` attribute set to the bibitem identifier. As with the Cite extension's `<ref>` tags, you can cite a named reference any number of times by using `<ref name="foo" />` on the page. For example,
<br />

```
I really learnt LaTeX from [<ref name="lamport94" />].

[<ref name="lamport94" />] is a great book.


== References ==

<references>
<bibitem>
\bibitem{lamport94}
  Leslie Lamport,
  \textit{\LaTeX: a document preparation system},
  Addison Wesley, Massachusetts,
  2nd edition,
  1994.
</bibitem>
</references>
```

will give you¹

![Screenshot](screenshots/BibitemCite_Lamport.png)

¹ The exact appearance of the reference links and text will depend on how you have configured the various template pages for the [Cite extension](https://www.mediawiki.org/wiki/Extension:Cite), of course.