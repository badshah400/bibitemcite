<?php

class BibitemCite {
	// Register any render callbacks with the parser
	public static function onParserSetup( Parser $parser ) {
		// When the parser sees the <bibitem> tag, it executes renderTagBibitem (see below)
		$parser->setHook( 'bibitem', 'BibitemCite::renderTagBibitem' );
	}

	// Render <bibitem>
	public static function renderTagBibitem( $input, array $args, Parser $parser, PPFrame $frame ) {
		$parser->disableCache();
		$lines = explode ("\n", $input);

		// Drop all commented or empty biblines
		$lines = preg_grep( "/^\s*%/", $lines, PREG_GREP_INVERT );
		$lines = preg_grep( "/^\s*$/", $lines, PREG_GREP_INVERT );

		foreach ( $lines as &$line ) {
			$line = trim($line);
			$line = rtrim($line, ".,");
		}

		$bibitemline = array_shift($lines);

		$c = preg_match ( "/\\\bibitem\s*\{(.*)\}/", $bibitemline, $bibitemline );

		if (! $c) {
			return htmlspecialchars($input);
		}

		$name = $bibitemline[1];

		$text = implode( ', ', $lines ) . '.';

		// Convert LaTeX non-breaking spaces into plain spaces
		$text = preg_replace( "/\.~/", ".", $text );
		$text = preg_replace( "/\.\\\ /", ". ", $text );

		// Convert LaTeX bold to wiki-bold
		// {\bf bold} or \textbf{bold} to '''bold'''
		$text = preg_replace( "/\{\\\bf\s+([^{}]+)\}/",      "'''$1'''", $text );
		$text = preg_replace( "/\\\\textbf\{([^{}]+)\}/", "'''$1'''", $text );

		// Convert LaTeX italics to wiki-italics
		// {\it italics} or \textit{italics} to ''italics''
		$text = preg_replace( "/\{\\\it\s+([^{}]+)\}/",      "''$1''", $text );
		$text = preg_replace( "/\\\\textit\{([^{}]+)\}/", "''$1''", $text );

		// Replace LaTeX-style quotes to ascii double quotes
		$text = preg_replace( "/``([^`'\"]*)''/", "\"$1\"", $text );

		$res = htmlspecialchars( $text );
		$output = $parser->recursiveTagParse( $res, $frame );

		// Replace LaTeX inline math by <math>, NEEDS TO BE DONE AFTER recursiveTagParse
		$output = preg_replace( "/\\\$([^$]+)\\\$/", "<math>$1</math>", $output );

		// Link to doi, NEEDS TO BE DONE AFTER recursiveTagParse TO CORRECTLY ADD HYPERLINKS
		// doi ID may contain any non-whitespace character except the comma...
		$output = preg_replace( "/doi:([^, ]*),/",   htmlspecialchars("[https://doi.org/$1") . " doi:$1],", $output );
		// ...but cannot end with a full-stop; if it does, the doi ID is the final bibitem text.
		$output = preg_replace( "/doi:([^, ]*)\.$/", htmlspecialchars("[https://doi.org/$1") . " doi:$1].", $output );

		// arXiv links, NEEDS TO BE DONE AFTER recursiveTagParse TO CORRECTLY ADD HYPERLINKS
		$output = preg_replace( "/\[arXiv:([a-z0-9\/-]+[\/.][0-9]+)(?:\s\[?[a-zA-Z.-]+\]?)\]/",
		                        htmlspecialchars("[https://arxiv.org/abs/$1") . " arXiv:$1]", $output );

		return $parser->recursivePreprocess('<ref name="' . htmlspecialchars($name) . '">' . $output . '</ref>', $frame);
	}
}
?>
